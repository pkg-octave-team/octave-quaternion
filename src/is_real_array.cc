/*

Copyright (C) 2010-2015   Lukas F. Reichlin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Return true if all arguments are real-valued arrays and false otherwise.

Author: Lukas Reichlin <lukas.reichlin@gmail.com>
Created: November 2011
Version: 0.3

*/

#include <octave/oct.h>

DEFUN_DLD (is_real_array, args, nargout,
   "-*- texinfo -*-\n\
@deftypefn {Loadable Function} {} is_real_array (@var{a}, @dots{})\n\
Return true if all arguments are real-valued arrays and false otherwise.\n\
@var{[]} is a valid array.\n\
Avoid nasty stuff like @code{true = isreal (\"a\")}.\n\
@end deftypefn")
{
    octave_value retval = true;
    octave_idx_type nargin = args.length ();

    if (nargin == 0)
    {
        print_usage ();
    }
    else
    {
        for (octave_idx_type i = 0; i < nargin; i++)
        {
            // args(i).ndims () should be always >= 2
            if (args(i).ndims () < 2
                || ! ((args(i).is_numeric_type () && args(i).is_real_type ())
                      || args(i).is_bool_type ()))
            {
                retval = false;
                break;
            }
        }
    }

    return retval;
}
