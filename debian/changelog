octave-quaternion (2.4.0-10) unstable; urgency=medium

  * d/copyright:
    + Accentuate my family name
    + Update Copyright years for debian/* files
  * Set upstream metadata fields: Archive.
  * d/control: Bump Standards-Version to 4.6.1 (no changes needed)
  * Build-depend on dh-sequence-octave
    + d/control: Ditto
    + d/rules: Drop the --with=octave option from dh call
  * d/watch: Adjust for new URL at gnu-octave.github.io
  * d/s/lintian-override: Override Lintian warning
    debian-watch-lacks-sourceforge-redirector

 -- Rafael Laboissière <rafael@debian.org>  Mon, 05 Dec 2022 10:47:28 -0300

octave-quaternion (2.4.0-9) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Sébastien Villemot <sebastien@debian.org>  Wed, 09 Dec 2020 18:18:57 +0100

octave-quaternion (2.4.0-8) experimental; urgency=medium

  * d/control: Bump Standards-Version to 4.5.1 (no changes needed)
  * d/p/build-against-octave-6.patch: New patch (Closes: #976203)

 -- Rafael Laboissière <rafael@debian.org>  Tue, 01 Dec 2020 20:12:53 -0300

octave-quaternion (2.4.0-7) unstable; urgency=medium

  * d/control:
    + Bump Standards-Version to 4.5.0 (no changes needed)
    + Bump debhelper compatibility level to 13
  * d/u/metadata: New file

 -- Rafael Laboissière <rafael@debian.org>  Wed, 29 Jul 2020 11:12:41 -0300

octave-quaternion (2.4.0-6) unstable; urgency=medium

  * d/control:
    + Bump Standards-Version to 4.4.1 (no changes needed)
    + Bump dependency on dh-octave to >= 0.7.1
      This allows the injection of the virtual package octave-abi-N
      into the package's list of dependencies.

 -- Rafael Laboissiere <rafael@debian.org>  Thu, 07 Nov 2019 19:00:32 -0300

octave-quaternion (2.4.0-5) unstable; urgency=medium

  [ Mike Miller ]
  * d/control, d/copyright: Use secure URL for upstream source.

  [ Rafael Laboissiere ]
  * d/control:
    + Add Rules-Requires-Root: no
    + Bump Standards-Version to 4.3.0
    + Bump to debhelper compat level 12
  * Build-depend on debhelper-compat instead of using d/compat

 -- Rafael Laboissiere <rafael@debian.org>  Wed, 02 Jan 2019 22:57:35 -0200

octave-quaternion (2.4.0-4) unstable; urgency=medium

  * Use dh-octave for building the package
  * d/control:
     + Use Debian's GitLab URLs in Vcs-* headers
     + Change Maintainer to team+pkg-octave-team@tracker.debian.org

 -- Rafael Laboissiere <rafael@debian.org>  Sat, 10 Feb 2018 07:37:27 -0200

octave-quaternion (2.4.0-3) unstable; urgency=medium

  * Use the dh-based version of octave-pkg-dev
  * Set debhelper compatibility level to >= 11
  * d/control:
    + Bump Standards-Version to 4.1.3 (no changes needed)
    + Add Testsuite field

 -- Rafael Laboissiere <rafael@debian.org>  Fri, 29 Dec 2017 22:14:28 -0200

octave-quaternion (2.4.0-2) unstable; urgency=medium

  [ Rafael Laboissiere ]
  * d/control:
    + Use secure URIs in the Vcs-* fields
    + Bump Standards-Version to 4.0.1 (no changes needed)
    + Use cgit instead of gitweb in Vcs-Browser URL

  [ Sébastien Villemot ]
  * d/copyright: use secure URL for format.
  * d/watch: bump to format version 4.

 -- Rafael Laboissiere <rafael@debian.org>  Mon, 21 Aug 2017 17:38:12 -0300

octave-quaternion (2.4.0-1) unstable; urgency=medium

  * Imported Upstream version 2.4.0
  * Bump Standards-Version to 3.9.6 (no changes needed)
  * d/copyright: Reflect upstream changes

 -- Rafael Laboissiere <rafael@laboissiere.net>  Sat, 12 Sep 2015 12:16:28 -0300

octave-quaternion (2.2.2-1) unstable; urgency=medium

  * Team upload.
  * Imported Upstream version 2.2.2
  * Uploaders: remove Thomas Weber and myself, add Rafael Laboissiere.

 -- Sébastien Villemot <sebastien@debian.org>  Wed, 06 Aug 2014 21:23:26 +0200

octave-quaternion (2.2.1-1) unstable; urgency=medium

  [ Rafael Laboissiere ]
  * Imported Upstream version 2.2.1

 -- Sébastien Villemot <sebastien@debian.org>  Tue, 13 May 2014 19:53:08 +0200

octave-quaternion (2.2.0-1) unstable; urgency=medium

  [ Rafael Laboissiere ]
  * Imported Upstream version 2.2.0
  * debian/copyright: Reflect upstream changes

 -- Sébastien Villemot <sebastien@debian.org>  Mon, 13 Jan 2014 19:22:26 +0100

octave-quaternion (2.0.3-1) unstable; urgency=low

  [ Sébastien Villemot ]
  * Imported Upstream version 2.0.3
  * debian/copyright: reflect upstream changes.
  * Bump Standards-Version to 3.9.5, no changes needed.

  [ Thomas Weber ]
  * debian/control: Use canonical URLs in Vcs-* fields

 -- Sébastien Villemot <sebastien@debian.org>  Wed, 06 Nov 2013 14:44:29 +0000

octave-quaternion (2.0.2-2) unstable; urgency=low

  [ Rafael Laboissiere ]
  * debian/copyright: Use the octave-maintainers mailing list as upstream
    contact

 -- Sébastien Villemot <sebastien@debian.org>  Mon, 20 May 2013 13:44:12 +0200

octave-quaternion (2.0.2-1) experimental; urgency=low

  [ Rafael Laboissiere ]
  * Imported Upstream version 2.0.2

 -- Sébastien Villemot <sebastien@debian.org>  Mon, 29 Oct 2012 20:06:40 +0100

octave-quaternion (2.0.1-1) experimental; urgency=low

  [ Rafael Laboissiere ]
  * Imported Upstream version 2.0.1
  * Drop patch add-doc-source.patch (applied upstream)
  * debian/copyright: Refresh for new upstream release
  * Bump Standards-Version to 3.9.4 (no changes needed)
  * Use Sébastien Villemot's @debian.org email address
  * Remove obsolete DM-Upload-Allowed flag

 -- Sébastien Villemot <sebastien@debian.org>  Mon, 15 Oct 2012 21:40:21 +0200

octave-quaternion (2.0.0-1) unstable; urgency=low

  [ Rafael Laboissiere ]
  * Initial release (closes: #529732)
  * debian/patches/add-doc-source.patch: New patch

 -- Thomas Weber <tweber@debian.org>  Mon, 02 Apr 2012 14:13:17 +0200
